//
//  ViewController.swift
//  G64L8
//
//  Created by Ivan Vasilevich on 7/30/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit


func log(_ functionName: String = #function, line: Int = #line, file: String = #file, message: String = "") {
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "HH:mm:ss"
	print("\(dateFormatter.string(from: Date())) l#:\(line) \(functionName) in \((file as NSString).lastPathComponent) " + message)
}


class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var textField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let stack0 = [1, 2, 1, ]
		let stack1 = [1, 2,]
		let stack2 = [1, ]
		
		let columns = [stack0, stack1, stack2]
		
		var color1 = UIColor.hexStringToUIColor(hex: "#b227d8")
		view.backgroundColor = color1
		foo()
		bar()
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let img = UIImage(named: "Location")
		imageView.image = #imageLiteral(resourceName: "Telegram")
		
		
		textField.font = UIFont.init(name:UIFont.fontNames(forFamilyName: UIFont.familyNames[3]).first!, size: 12)
	}
	
	func foo() {
//		print("foo called")
		log()
	}
	
	func bar() {
//		print("bar called")
		log()
	}
	

}

