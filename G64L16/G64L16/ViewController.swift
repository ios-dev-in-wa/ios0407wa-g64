//
//  ViewController.swift
//  G64L16
//
//  Created by Ivan Vasilevich on 8/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import UserNotifications


class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		requestNotifications()
	}
	
	func requestNotifications() {
		// Request Notification Settings
		UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
			switch notificationSettings.authorizationStatus {
			case .notDetermined:
				self.requestAuthorization(completionHandler: { (success) in
					guard success else { return }
					
					// Schedule Local Notification
					self.scheduleLocalNotification()
				})
			case .authorized:
				// Schedule Local Notification
				self.scheduleLocalNotification()
			case .denied:
				print("Application Not Allowed to Display Notifications")
			}
		}
	}
	
	
	private func scheduleLocalNotification() {
		// Create Notification Content
		let notificationContent = UNMutableNotificationContent()
		
		// Configure Notification Content
		notificationContent.title = "Cocoacasts"
		notificationContent.subtitle = "Local Notifications"
		notificationContent.body = "In this tutorial, you learn how to schedule local notifications with the User Notifications framework."
		
		// Add Trigger
		let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
		
		// Create Notification Request
		let notificationRequest = UNNotificationRequest(identifier: "cocoacasts_local_notification", content: notificationContent, trigger: notificationTrigger)
		
		// Add Request to User Notification Center
		UNUserNotificationCenter.current().add(notificationRequest) { (error) in
			if let error = error {
				print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
			}
		}
	}
	
	// MARK: - Private Methods
	
	private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
		// Request Authorization
		UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
			if let error = error {
				print("Request Authorization Failed (\(error), \(error.localizedDescription))")
			}
			
			completionHandler(success)
		}
	}
	
	
//	func setupNotificationReminder() {
//		var title:String = "Your reminder text goes here"
//
//		var calendar = NSCalendar.current
//		var calendarComponents = NSDateComponents()
//		calendarComponents.hour = 7
//		calendarComponents.second = 0
//		calendarComponents.minute = 0
//		calendar.timeZone = NSTimeZone.default
//		var dateToFire = calendar.date(from: calendarComponents as DateComponents)
//
//		// create a corresponding local notification
//		let notification = UILocalNotification()
//
//		let dict:NSDictionary = ["ID" : "your ID goes here"]
//		notification.userInfo = dict as! [String : String]
//		notification.alertBody = "\(title)"
//		notification.alertAction = "Open"
//		notification.fireDate = dateToFire
//		notification.repeatInterval = .Day  // Can be used to repeat the notification
//		notification.soundName = UILocalNotificationDefaultSoundName
//		UIApplication.sharedApplication().scheduleLocalNotification(notification)
//	}
	
	
}

