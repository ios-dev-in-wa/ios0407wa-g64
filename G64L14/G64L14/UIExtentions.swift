import UIKit
import CoreImage
import UserNotifications
import Accelerate
import MapKit

func log(_ functionName: String = #function, line: Int = #line, file: String = #file, message: String = "") {
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "HH:mm:ss"
	print("---\(dateFormatter.string(from: Date())) l#:\(line) \(functionName) in \((file as NSString).lastPathComponent) " + message)
}
// Threads.
let kBgQ = DispatchQueue.global(qos: .background)
let kMainQueue = DispatchQueue.main

typealias CCImage = CIImage

extension UIViewController {
	@objc func disbleTabBar() {
		self.navigationController?.tabBarController?.tabBar.isUserInteractionEnabled = false
	}
}

extension UIColor {
	
	convenience init(red: Int, green: Int, blue: Int) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")
		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
	}
	
	convenience init(netHex:Int) {
		self.init(red:CGFloat((netHex >> 16) & 0xff) / 255.0,
		                    green:CGFloat((netHex >> 8) & 0xff) / 255.0,
		                    blue:CGFloat(netHex & 0xff) / 255.0,
		                    alpha: 1)
	}
	
	convenience init(rgb: UInt, alphaVal: CGFloat) {
		self.init(
			red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgb & 0x0000FF) / 255.0,
			alpha: CGFloat(alphaVal)
		)
	}
	
	static func randomColor() -> UIColor {
		return UIColor(
			red: CGFloat(arc4random_uniform(256))/255,
			green: CGFloat(arc4random_uniform(256))/255,
			blue: CGFloat(arc4random_uniform(256))/255,
			alpha: 1)
	}
    
    static let darkBlueFan = UIColor(netHex: 0x297EF0)
	
    static let lightBlueFan = UIColor(netHex: 0x4ECBF4)
    
    static let lightGrayFan = UIColor(netHex: 0x87909B)
    
    static let deepDarkBlueFan = UIColor(netHex: 0x0D254C)
    
    static let turquoiseFan =  UIColor(netHex: 0x56CCF2)
	
    static var blueFan = UIColor(netHex: 0x2F80ED)
	
	static let blueWhiteGradientTop = UIColor.init(displayP3Red: 0.36, green: 0.51, blue: 0.9, alpha: 1)
	static let blueWhiteGradientBottom = UIColor.init(displayP3Red: 0.58, green: 0.78, blue: 0.94, alpha: 1)//58 78 94 // 36 51 90
	
}

public extension CIColor {
	
	/// Creates a CIColor from an rgba string
	///
	/// E.g.
	///     `aaa`
	///     `ff00`
	///     `bb00ff`
	///     `aabbccff`
	///
	/// - parameter rgba:    The hex string to parse in rgba format
	public convenience init(rgba: String) {
		var r: CGFloat = 0.0
		var g: CGFloat = 0.0
		var b: CGFloat = 0.0
		var a: CGFloat = 1.0
		
		let scanner = Scanner(string: rgba)
		var hexValue: CUnsignedLongLong = 0
		
		if scanner.scanHexInt64(&hexValue) {
			let length = rgba.characters.count
			
			switch (length) {
			case 3:
				r = CGFloat((hexValue & 0xF00) >> 8)    / 15.0
				g = CGFloat((hexValue & 0x0F0) >> 4)    / 15.0
				b = CGFloat(hexValue & 0x00F)           / 15.0
			case 4:
				r = CGFloat((hexValue & 0xF000) >> 12)  / 15.0
				g = CGFloat((hexValue & 0x0F00) >> 8)   / 15.0
				b  = CGFloat((hexValue & 0x00F0) >> 4)  / 15.0
				a = CGFloat(hexValue & 0x000F)          / 15.0
			case 6:
				r = CGFloat((hexValue & 0xFF0000) >> 16)    / 255.0
				g = CGFloat((hexValue & 0x00FF00) >> 8)     / 255.0
				b  = CGFloat(hexValue & 0x0000FF)           / 255.0
			case 8:
				r = CGFloat((hexValue & 0xFF000000) >> 24)  / 255.0
				g = CGFloat((hexValue & 0x00FF0000) >> 16)  / 255.0
				b = CGFloat((hexValue & 0x0000FF00) >> 8)   / 255.0
				a = CGFloat(hexValue & 0x000000FF)          / 255.0
			default:
				print("Invalid number of values (\(length)) in HEX string. Make sure to enter 3, 4, 6 or 8 values. E.g. `aabbccff`")
			}
			
		} else {
			print("Invalid HEX value: \(rgba)")
		}
		
		self.init(red: r, green: g, blue: b, alpha: a)
	}
	
}

internal typealias Scale = (dx: CGFloat, dy: CGFloat)

internal extension CIImage {
	/// Creates an `UIImage` with interpolation disabled and scaled given a scale property
	///
	/// - parameter withScale:  a given scale using to resize the result image
	///
	/// - returns: an non-interpolated UIImage
	internal func nonInterpolatedImage(withScale scale: Scale = Scale(dx: 1, dy: 1)) -> UIImage? {
		guard let cgImage = CIContext(options: nil).createCGImage(self, from: self.extent) else { return nil }
		let size = CGSize(width: self.extent.size.width * scale.dx, height: self.extent.size.height * scale.dy)
		
		UIGraphicsBeginImageContextWithOptions(size, true, 0)
		guard let context = UIGraphicsGetCurrentContext() else { return nil }
		context.interpolationQuality = .none
		context.translateBy(x: 0, y: size.height)
		context.scaleBy(x: 1.0, y: -1.0)
		context.draw(cgImage, in: context.boundingBoxOfClipPath)
		let result = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return result
	}
}

extension UIImage {
	// reduce size to 50KB
	class func reduceSizeOf(image: UIImage) -> Data {
		var newImage = image
		var compression: CGFloat = 1
		var imgData = UIImageJPEGRepresentation(newImage, compression)!
		while imgData.count > 51200 {
			if newImage.size.width > CGFloat(500) && newImage.size.height > CGFloat(800) {
				if newImage.size.width/newImage.size.height > 0 {
					// If width > height
					let multiplier = 800 / newImage.size.height
					newImage = newImage.scaledToWidth(multiplier*newImage.size.width)
				} else {
					newImage = newImage.scaledToWidth(500)
				}
			} else {
				guard compression > 0.0 else {break}
				imgData = UIImageJPEGRepresentation(newImage, compression)!
				compression -= 0.1
			}
		}
		return imgData
	}
	
	func resizeImage(newHeight: CGFloat) -> UIImage {
		let image = self;
		let scale = newHeight / image.size.height
		let newWidth = image.size.width * scale
		UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
		image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return newImage!
	}
	
	convenience init(view: UIView) {
		UIGraphicsBeginImageContext(view.frame.size)
		view.layer.render(in: UIGraphicsGetCurrentContext()!)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		self.init(cgImage: (image?.cgImage)!)
	}
	
	func scaledToWidth(_ scaledToWidth: CGFloat) -> UIImage! {
		let oldWidth = self.size.width
		let scaleFactor = scaledToWidth / oldWidth
		
		let newHeight = self.size.height * scaleFactor
		let newWidth = oldWidth * scaleFactor
		
		UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
		self.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return newImage!
	}
	
	func scaledToHeight(_ scaledToHeight: CGFloat) -> UIImage! {
		let oldHeight = self.size.height
		let scaleFactor = scaledToHeight / oldHeight
		
		let newHeight = self.size.height * scaleFactor
		let newWidth = oldHeight * scaleFactor
		
		UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
		self.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return newImage!
	}
	
	func scaledAspect(_ size: CGSize) -> UIImage! { //10x15
		
		var ratio = CGFloat(0)
		
		if (self.size.width > self.size.height) {
			ratio = CGFloat(1000) / self.size.width;
		} else {
			ratio = CGFloat(1000) / self.size.height;
		}
		let newWidth = self.size.width * ratio;
		let  newHeight = self.size.height * ratio;
		
		
		
		var drawBox = self.size //150x100
		if drawBox.width >= size.width && drawBox.height >= size.height { //true
			
			while drawBox.width > newWidth || drawBox.height > newHeight {
				drawBox.width *= 0.9
				drawBox.height *= 0.9
			}
			
		}
		
		
		
		
		UIGraphicsBeginImageContext(drawBox)
		self.draw(in: CGRect(x: 0, y: 0, width: drawBox.width, height: drawBox.height))
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext();
		return newImage;
	}
	
	func roundedImage(_ size: CGSize) -> UIImage! {
		// Get your image somehow
		let image = self
		
		// Begin a new image that will be the new image with the rounded corners
		// (here with the size of an UIImageView)
		UIGraphicsBeginImageContextWithOptions(size, false, 1.0);
		
		// Add a clip before drawing anything, in the shape of an rounded rect
		let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: size.height), cornerRadius: size.width/2) //bezierPathWithRoundedRect:imageView.bounds
		//cornerRadius:10.0] addClip];
		path.addClip()
		// Draw your image
		image.draw(in: path.bounds)
		//        [image drawInRect:imageView.bounds];
		path.lineWidth *= 2
		//        UIColor(red: 63/255.0, green: 255/255.0, blue: 161/255.0, alpha: 1).setStroke()
		UIColor.black.setStroke()
		path.stroke()
		// Get the image, here setting the UIImageView image
		let result = UIGraphicsGetImageFromCurrentImageContext();
		
		// Lets forget about that we were drawing
		UIGraphicsEndImageContext();
		
		return result
	}
	
	func tintedBackgroundImageWithColor(_ tintColor: UIColor) -> UIImage {
		UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
		tintColor.setFill()
		let bounds = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
		UIRectFill(bounds)
		self.draw(in: bounds, blendMode: .sourceAtop, alpha: 1)
		
		let tintedImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		return tintedImage!;
	}
	
	func scale(image originalImage: UIImage, toLessThan maxResolution: CGFloat) -> UIImage? {
		guard let imageReference = originalImage.cgImage else { return nil }
		
		let rotate90 = CGFloat.pi/2.0 // Radians
		let rotate180 = CGFloat.pi // Radians
		let rotate270 = 3.0*CGFloat.pi/2.0 // Radians
		
		let originalWidth = CGFloat(imageReference.width)
		let originalHeight = CGFloat(imageReference.height)
		let originalOrientation = originalImage.imageOrientation
		
		var newWidth = originalWidth
		var newHeight = originalHeight
		
		if originalWidth > maxResolution || originalHeight > maxResolution {
			let aspectRatio: CGFloat = originalWidth / originalHeight
			newWidth = aspectRatio > 1 ? maxResolution : maxResolution * aspectRatio
			newHeight = aspectRatio > 1 ? maxResolution / aspectRatio : maxResolution
		}
		
		let scaleRatio: CGFloat = newWidth / originalWidth
		var scale: CGAffineTransform = .init(scaleX: scaleRatio, y: -scaleRatio)
		scale = scale.translatedBy(x: 0.0, y: -originalHeight)
		
		var rotateAndMirror: CGAffineTransform
		
		switch originalOrientation {
		case .up:
			rotateAndMirror = .identity
			
		case .upMirrored:
			rotateAndMirror = .init(translationX: originalWidth, y: 0.0)
			rotateAndMirror = rotateAndMirror.scaledBy(x: -1.0, y: 1.0)
			
		case .down:
			rotateAndMirror = .init(translationX: originalWidth, y: originalHeight)
			rotateAndMirror = rotateAndMirror.rotated(by: rotate180 )
			
		case .downMirrored:
			rotateAndMirror = .init(translationX: 0.0, y: originalHeight)
			rotateAndMirror = rotateAndMirror.scaledBy(x: 1.0, y: -1.0)
			
		case .left:
			(newWidth, newHeight) = (newHeight, newWidth)
			rotateAndMirror = .init(translationX: 0.0, y: originalWidth)
			rotateAndMirror = rotateAndMirror.rotated(by: rotate270)
			scale = .init(scaleX: -scaleRatio, y: scaleRatio)
			scale = scale.translatedBy(x: -originalHeight, y: 0.0)
			
		case .leftMirrored:
			(newWidth, newHeight) = (newHeight, newWidth)
			rotateAndMirror = .init(translationX: originalHeight, y: originalWidth)
			rotateAndMirror = rotateAndMirror.scaledBy(x: -1.0, y: 1.0)
			rotateAndMirror = rotateAndMirror.rotated(by: rotate270)
			
		case .right:
			(newWidth, newHeight) = (newHeight, newWidth)
			rotateAndMirror = .init(translationX: originalHeight, y: 0.0)
			rotateAndMirror = rotateAndMirror.rotated(by: rotate90)
			scale = .init(scaleX: -scaleRatio, y: scaleRatio)
			scale = scale.translatedBy(x: -originalHeight, y: 0.0)
			
		case .rightMirrored:
			(newWidth, newHeight) = (newHeight, newWidth)
			rotateAndMirror = .init(scaleX: -1.0, y: 1.0)
			rotateAndMirror = rotateAndMirror.rotated(by: CGFloat.pi/2.0)
		}
		
		UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
		guard let context = UIGraphicsGetCurrentContext() else { return nil }
		context.concatenate(scale)
		context.concatenate(rotateAndMirror)
		context.draw(imageReference, in: CGRect(x: 0, y: 0, width: originalWidth, height: originalHeight))
		let copy = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return copy
	}
	
	func tint(tintColor: UIColor) -> UIImage {
		
		return modifiedImage(draw: { context, rect in
			// draw black background - workaround to preserve color of partially transparent pixels
			context.setBlendMode(.normal)
			UIColor.black.setFill()
			context.fill(rect)
			
			// draw original image
			context.setBlendMode(.normal)
			context.draw(self.cgImage!, in: CGRect(x: 0.0,y: 0.0,width: self.size.width,height: self.size.height))
			
			
			// tint image (loosing alpha) - the luminosity of the original image is preserved
			context.setBlendMode(.color)
			tintColor.setFill()
			context.fill(rect)
			
			// mask by alpha values of original image
			context.setBlendMode(.destinationIn)
			context.draw(self.cgImage!, in: CGRect(x: 0.0,y: 0.0,width: self.size.width,height: self.size.height))
		})
	}
	
	// fills the alpha channel of the source image with the given color
	// any color information except to the alpha channel will be ignored
	//	func fillAlpha(fillColor: UIColor) -> UIImage {
	//
	//		return modifiedImage { context, rect in
	//			// draw tint color
	//			CGContextSetBlendMode(context, .Normal)
	//			fillColor.setFill()
	//			CGContextFillRect(context, rect)
	//
	//			// mask by alpha values of original image
	//			CGContextSetBlendMode(context, .DestinationIn)
	//			CGContextDrawImage(context, rect, self.CGImage)
	//		}
	//	}
	
	private func modifiedImage( draw: (CGContext, CGRect) -> ()) -> UIImage {
		
		// using scale correctly preserves retina images
		UIGraphicsBeginImageContextWithOptions(size, false, scale)
		let context: CGContext! = UIGraphicsGetCurrentContext()
		assert(context != nil)
		
		// correctly rotate image
		context.translateBy(x: 0, y: size.height);
		context.scaleBy(x: 1.0, y: -1.0);
		
		let rect = CGRect(x:0.0, y:0.0, width:size.width, height:size.height)
		
		draw(context, rect)
		
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return image!
	}
	
	/**
	Tint, Colorize image with given tint color<br><br>
	This is similar to Photoshop's "Color" layer blend mode<br><br>
	This is perfect for non-greyscale source images, and images that have both highlights and shadows that should be preserved<br><br>
	white will stay white and black will stay black as the lightness of the image is preserved<br><br>
	
	<img src="http://yannickstephan.com/easyhelper/tint1.png" height="70" width="120"/>
	
	**To**
	
	<img src="http://yannickstephan.com/easyhelper/tint2.png" height="70" width="120"/>
	
	- parameter tintColor: UIColor
	
	- returns: UIImage
	*/
	func tintPhoto(_ tintColor: UIColor) -> UIImage {
		
		return modifiedImage(draw: { context, rect in
			// draw black background - workaround to preserve color of partially transparent pixels
			context.setBlendMode(.normal)
			UIColor.black.setFill()
			context.fill(rect)
			
			// draw original image
			context.setBlendMode(.normal)
			context.draw(cgImage!, in: rect)
			
			// tint image (loosing alpha) - the luminosity of the original image is preserved
			context.setBlendMode(.color)
			tintColor.setFill()
			context.fill(rect)
			
			// mask by alpha values of original image
			context.setBlendMode(.destinationIn)
			context.draw(context.makeImage()!, in: rect)
		})
	}
	
	/**
	Tint Picto to color
	
	- parameter fillColor: UIColor
	
	- returns: UIImage
	*/
	func tintPicto(_ fillColor: UIColor) -> UIImage {
		
		return modifiedImage(draw: { context, rect in
			// draw tint color
			context.setBlendMode(.normal)
			fillColor.setFill()
			context.fill(rect)
			
			// mask by alpha values of original image
			context.setBlendMode(.destinationIn)
			context.draw(cgImage!, in: rect)
		})
	}
	
	/**
	Modified Image Context, apply modification on image
	
	- parameter draw: (CGContext, CGRect) -> ())
	
	- returns: UIImage
	*/
	fileprivate func modifiedImage(_ draw: (CGContext, CGRect) -> ()) -> UIImage {
		
		// using scale correctly preserves retina images
		UIGraphicsBeginImageContextWithOptions(size, false, scale)
		let context: CGContext! = UIGraphicsGetCurrentContext()
		assert(context != nil)
		
		// correctly rotate image
		context.translateBy(x: 0, y: size.height)
		context.scaleBy(x: 1.0, y: -1.0)
		
		let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
		
		draw(context, rect)
		
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return image!
	}
	
	func inverseImage(cgResult: Bool) -> UIImage? {
		let coreImage = UIKit.CIImage(image: self)
		guard let filter = CIFilter(name: "CIColorInvert") else { return nil }
		filter.setValue(coreImage, forKey: kCIInputImageKey)
		guard let result = filter.value(forKey: kCIOutputImageKey) as? UIKit.CIImage else { return nil }
		if cgResult { // I've found that UIImage's that are based on CIImages don't work with a lot of calls properly
			return UIImage(cgImage: CIContext(options: nil).createCGImage(result, from: result.extent)!)
			//			return UIImage(CGImage: CIContext(options: nil).createCGImage(result, fromRect: result.extent)!)
		}
		return UIImage(ciImage: result)
	}
	
}

extension UIImageView {
	var imageA: UIImage? {
		get {
			return image
		}
		set {
			UIView.transition(with: self,
			                  duration: 0.3,
			                  options: .transitionCrossDissolve,
			                  animations: { self.image = newValue;  },
			                  completion: nil)
			
		}
	}
}

extension UIButton {
	func adjustImageAndTitleOffsets() {
		guard self.imageView != nil else {return}
		let spacing: CGFloat = 6.0
		
		let imageSize = self.imageView!.frame.size
		
		titleEdgeInsets = UIEdgeInsetsMake(0, -imageSize.width, -(imageSize.height + spacing), 0)
		
		let titleSize = titleLabel!.frame.size
		
		imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0, 0, -titleSize.width)
	}
}

extension UIFont {
	
	func withTraits(traits:UIFontDescriptorSymbolicTraits...) -> UIFont {
		let descriptor = self.fontDescriptor
			.withSymbolicTraits(UIFontDescriptorSymbolicTraits(traits))
		return UIFont(descriptor: descriptor!, size: 0)
	}
	
	func bold() -> UIFont {
		return withTraits(traits: .traitBold)
	}
	
	func regular() -> UIFont {
		return withTraits(traits: .traitBold)
	}
	
}

extension UIView {
	func becomeFront() {
		kMainQueue.async {
			self.superview?.bringSubview(toFront: self)
		}
	}
	
	func moveBack() {
		kMainQueue.async {
			self.superview?.sendSubview(toBack: self)
		}
	}
	
	func copyView() -> UIView
	{
		return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self))! as! UIView
	}
	
	@IBInspectable var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
	}
	
	@IBInspectable var borderColor: UIColor? {
		get {
			return UIColor(cgColor: self.layer.borderColor!)
		}
		set {
			self.layer.borderColor = newValue?.cgColor
		}
	}
	
	@IBInspectable var borderWidth: CGFloat {
		get {
			return self.layer.borderWidth
		}
		set {
			self.layer.borderWidth = newValue
		}
	}
	
	
	func addGradientBackgound(firstColor: UIColor, SecondColor: UIColor, topToBottom: Bool) {
		self.layoutIfNeeded()
		self.removeGradients()
        let gradient: CAGradientLayer = CAGradientLayer()
		gradient.colors = [firstColor.cgColor, SecondColor.cgColor]
		gradient.locations = [0.0 , 0.5]
		gradient.startPoint = CGPoint(x: 1.0, y: topToBottom ? 0.0 : 1.0)
		gradient.endPoint = CGPoint(x: topToBottom ? 1.0 : 0.0, y: 1.0)
		gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.size.width + 2, height: self.bounds.size.height)
		gradient.name = "grad"
		self.layer.insertSublayer(gradient, at: 0)
	}
	
	func removeGradients() {
		self.layer.sublayers?.forEach({ (layer) in
			if layer.name == "grad" {
				layer.removeFromSuperlayer()
			}
		})
	}
	
    func round(corners: UIRectCorner, radii: Int, withBorder: UIColor? = nil) {
		let path = UIBezierPath(roundedRect: self.bounds,
		                        byRoundingCorners: corners,
		                        cornerRadii: CGSize(width: radii, height:  radii))
		let maskLayer = CAShapeLayer()
		maskLayer.path = path.cgPath
		self.layer.mask = maskLayer
        
        if withBorder != nil {
            let borderLayer = CAShapeLayer()
            borderLayer.path = path.cgPath
            borderLayer.fillColor = UIColor.clear.cgColor
            borderLayer.strokeColor = withBorder!.cgColor
            borderLayer.lineWidth = 2
            borderLayer.frame = self.bounds
            self.layer.sublayers?.forEach{ if $0.isKind(of: CAShapeLayer.self) { $0.removeFromSuperlayer() } }
            self.layer.addSublayer(borderLayer)
        }
	}
}

extension UIUserNotificationType {
	
	@available(iOS 10.0, *)
	func authorizationOptions() -> UNAuthorizationOptions {
		var options: UNAuthorizationOptions = []
		if contains(.alert) {
			options.formUnion(.alert)
		}
		if contains(.sound) {
			options.formUnion(.sound)
		}
		if contains(.badge) {
			options.formUnion(.badge)
		}
		return options
	}
}

extension UILabel{
	func addTextSpacing(spacing: CGFloat){
		let attributedString = NSMutableAttributedString(string: self.text!)
		attributedString.addAttribute(NSAttributedStringKey.kern, value: spacing, range: NSRange(location: 0, length: self.text!.characters.count))
		self.attributedText = attributedString
	}
}

extension UIViewController {
	
	func lockView() {
		let darkView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
		darkView.tag = 321
		darkView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
		
		let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
		spinner.tag = 321
		spinner.startAnimating()
		spinner.center = darkView.center
		
		if let superView = view.superview {
			superView.addSubview(darkView)
			superView.addSubview(spinner)
		} else {
			view.addSubview(darkView)
			view.addSubview(spinner)
		}
		UIApplication.shared.beginIgnoringInteractionEvents()
	}
	
	func unlockView() {
		UIApplication.shared.endIgnoringInteractionEvents()
		if let superView = view.superview {
			superView.subviews.forEach{ if $0.tag == 321 {$0.removeFromSuperview()} }
		} else {
			view.subviews.forEach{ if $0.tag == 321 {$0.removeFromSuperview()} }
		}
	}
	
	func shake(_ view: UIView) {
		let animation = CABasicAnimation(keyPath: "position")
		animation.duration = 0.07
		animation.repeatCount = 3
		animation.autoreverses = true
		animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
		animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
		view.layer.add(animation, forKey: "position")
	}
	
}

extension NSLayoutConstraint {
	func constraintWithMultiplier(multiplier: CGFloat) -> NSLayoutConstraint {
		return NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
	}
}

extension UINavigationController {
	var previousViewController: UIViewController? {
		if viewControllers.count > 1 {
			return viewControllers[viewControllers.count - 2]
		}
		return nil
	}
    
    func pushDissolve(viewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.view.layer.add(transition, forKey: nil)
        self.pushViewController(viewController, animated: false)
    }
    
    func popDissolve() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.view.layer.add(transition, forKey: nil)
        self.popViewController(animated: false)
    }
    
    func popToRootDissolve() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.view.layer.add(transition, forKey: nil)
        self.popToRootViewController(animated: false)
    }
    
}

extension CLLocationCoordinate2D {
	// In meteres
	static func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
		let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
		let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
		return from.distance(from: to)
	}
}

extension UIApplication {
	
	class func appVersion() -> String {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
	}
	
	class func appBuild() -> String {
		return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
	}
	
	class func versionBuild() -> String {
		let version = appVersion(), build = appBuild()
		
		return version == build ? "v\(version)" : "v\(version)(\(build))"
	}
}

public extension UIWindow {
	public var visibleViewController: UIViewController? {
		return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
	}
	
	public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
		if let nc = vc as? UINavigationController {
			return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
		} else if let tc = vc as? UITabBarController {
			return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
		} else {
			if let pvc = vc?.presentedViewController {
				return UIWindow.getVisibleViewControllerFrom(pvc)
			} else {
				return vc
			}
		}
	}
}

extension UIDevice {
	
	var modelName: String {
		var systemInfo = utsname()
		uname(&systemInfo)
		let machineMirror = Mirror(reflecting: systemInfo.machine)
		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else { return identifier }
			return identifier + String(UnicodeScalar(UInt8(value)))
		}
		
		switch identifier {
		case "iPod5,1":                                 return "iPod Touch 5"
		case "iPod7,1":                                 return "iPod Touch 6"
		case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
		case "iPhone4,1":                               return "iPhone 4s"
		case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
		case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
		case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
		case "iPhone7,2":                               return "iPhone 6"
		case "iPhone7,1":                               return "iPhone 6 Plus"
		case "iPhone8,1":                               return "iPhone 6s"
		case "iPhone8,2":                               return "iPhone 6s Plus"
		case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
		case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
		case "iPhone8,4":                               return "iPhone SE"
		case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
		case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
		case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
		case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
		case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
		case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
		case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
		case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
		case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
		case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
		case "AppleTV5,3":                              return "Apple TV"
		case "i386", "x86_64":                          return "Simulator"
		default:                                        return identifier
		}
	}
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4 = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}
