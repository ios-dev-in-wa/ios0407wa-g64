//
//  ViewController.swift
//  G64L14
//
//  Created by Ivan Vasilevich on 8/20/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {
	
	@IBOutlet weak var centerLabel: UILabel!
	@IBOutlet weak var centerLabelHorizontalConstraint: NSLayoutConstraint!
	
	var timer: Timer?
	let motionManager = CMMotionManager()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		motionManager.startAccelerometerUpdates()
		
		timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(foo), userInfo: nil, repeats: true)
		
		let box = FaceView(frame: CGRect(x: 10, y: 10, width: 200, height: 200))
//		box.faceColor = .yellow
//		box.happyLevel = 30
		box.changeHappyFace(withHappLevel: 38, andColor: .blue)
		view.addSubview(box)
		box.tag = 666
	}
	
	@objc func foo() {
		let screenWidthHalf = view.frame.width / 2
		if let data = motionManager.accelerometerData {
				// 4 How do you move the label?
				centerLabel.text = "\(data.acceleration.x)"
			centerLabelHorizontalConstraint.constant = CGFloat(data.acceleration.x) * screenWidthHalf
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let alert = UIAlertController(title: "T", message: "M", preferredStyle: .actionSheet)
		
		let dismissAction = UIAlertAction(title: "Dismiss", style: .destructive) { (act) in
			print("dismiss pressed")
		}
		
		alert.addAction(dismissAction)
		
//		present(alert, animated: true, completion: nil)
		
		let firstTouch = touches.first!
		let touchLoc = firstTouch.location(in: view)
		
		if let morda = view.viewWithTag(666) {
//			UIView.animate(withDuration: 0.3) {
//				morda.center = touchLoc
//			}
			UIView.animate(withDuration: 0.3, delay: 1, options: [ UIViewAnimationOptions.autoreverse], animations: {
				morda.center = touchLoc
			}) { (completed) in
				if completed {
					self.view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
														green: CGFloat(arc4random_uniform(256))/255,
														blue: CGFloat(arc4random_uniform(256))/255,
														alpha: 1)
				}
			}
		}
		
	}


}

