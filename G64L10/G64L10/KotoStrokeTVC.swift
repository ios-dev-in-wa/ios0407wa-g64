//
//  KotoStrokeTVC.swift
//  G64L10
//
//  Created by Ivan Vasilevich on 8/6/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class KotoStrokeTVC: UITableViewController {

	override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		Cat.shared.stroke()
	}

}
