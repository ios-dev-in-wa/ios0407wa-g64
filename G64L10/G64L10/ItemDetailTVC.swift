//
//  ItemDetailTVC.swift
//  G64L10
//
//  Created by Ivan Vasilevich on 8/8/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ItemDetailTVC: UITableViewController {

	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var profileLabel: UILabel!
	var staffToDisplay: kakayetoHrenIzABC!
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		profileLabel.text = staffToDisplay.index.description
		profileImageView.image = staffToDisplay.picture
	}

}
