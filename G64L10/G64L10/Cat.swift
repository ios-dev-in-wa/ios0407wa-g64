//
//  Cat.swift
//  G64L10
//
//  Created by Ivan Vasilevich on 8/6/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Cat: NSObject {
	
	static let shared = Cat()
	
	private var amountOfStrokes = 0
	
	func stroke() {
		amountOfStrokes += 1
		print("amountOfStrokes = \(amountOfStrokes)")
	}

}
