//
//  ViewController.swift
//  G64L10
//
//  Created by Ivan Vasilevich on 8/6/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

typealias kakayetoHrenIzABC = (index: Int, picture: UIImage)

class ViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	var images = [#imageLiteral(resourceName: "alligator"), #imageLiteral(resourceName: "alligator"), #imageLiteral(resourceName: "alligator")]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		tabBarController?.viewControllers
		tableView.dataSource = self
		tableView.delegate = self
		
		let nib = UINib(nibName: "AlligatorCellTableViewCell", bundle: nil)
		tableView.register(nib, forCellReuseIdentifier: "12345")
		tableView.rowHeight = 100
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		Cat.shared.stroke()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		print(sender as! NSObject)
		if let selectedIndexRow = tableView.indexPathForSelectedRow?.row
		{
			let tupleToDisplay = (selectedIndexRow, images[selectedIndexRow])
			if let itemDetailTVC = segue.destination as? ItemDetailTVC {
				itemDetailTVC.staffToDisplay = tupleToDisplay
			}
		}
	}

	// MARK: - IBActions
	@IBAction func pickImage(_ sender: UIBarButtonItem) {
		let picker = UIImagePickerController()
		picker.delegate = self
		present(picker, animated: true, completion: nil)
	}
	
	func foo(argh: kakayetoHrenIzABC) -> kakayetoHrenIzABC {
		return (1, #imageLiteral(resourceName: "alligator"))
	}

}

extension ViewController: UITableViewDataSource {
	// MARK: - TableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return images.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "12345", for: indexPath) as! AlligatorCellTableViewCell
		let index = indexPath.row
		let image = images[index]
		cell.centerImageView?.image = image
		cell.numberLabel.text = (index + 1).description
		return cell
	}
}

extension ViewController: UITableViewDelegate {
	// MARK: - TableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(indexPath)
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		return indexPath.row != 1 ? 100 : 0
	}
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	// MARK: - UIImagePickerControllerDelegate
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		print(info)
		let img = info[UIImagePickerControllerOriginalImage] as! UIImage
		images.append(img)
		if images.count > 3 {
			picker.dismiss(animated: true, completion: nil)
			tableView.reloadData()
		}
		
	}
	
}

