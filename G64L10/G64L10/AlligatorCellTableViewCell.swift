//
//  AlligatorCellTableViewCell.swift
//  G64L10
//
//  Created by Ivan Vasilevich on 8/8/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class AlligatorCellTableViewCell: UITableViewCell {
	
	@IBOutlet weak var centerImageView: UIImageView!
	
	@IBOutlet weak var numberLabel: UILabel!
	
}
