//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
	
	func l0c() {
		move()
		if frontIsClear {
			doubleMove()
			pick()
			doubleMove()
			turnRight()
			move()
			put()
			turnLeft()
			doubleMove()
		}
		else {
			turnRight()
		}
	}
	
	func forLoopExample() {
		for _ in 0..<11120 {
			if frontIsClear {
				put()
				move()
			}
			else {
				break
			}
			
		}
	}
	
	func whileLoopExample() {
		while frontIsClear {
			put()
			move()
		}
		
		while noCandyPresent {
			if frontIsClear {
				move()
			}
			else {
				break
			}
		}
	}
	
	//in this function change levelName
	override func viewDidLoad() {
		levelName = "L555H" // level name
		
		super.viewDidLoad()
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		super.viewDidAppear(animated)
		/**/
		//пока нету крнфеты
		
		put()
		
		
		
		
	}
	
	func turnLeft() {
		turnRight()
		turnRight()
		turnRight()
	}
	
	func doubleMove() {
		move()
		move()
	}
	
	
}
