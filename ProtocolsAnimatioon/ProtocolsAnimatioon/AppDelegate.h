//
//  AppDelegate.h
//  ProtocolsAnimatioon
//
//  Created by Ivan Vasilevich on 11/23/14.
//  Copyright (c) 2014 Ivan Vasilevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

