//
//  main.m
//  ProtocolsAnimatioon
//
//  Created by Ivan Vasilevich on 11/23/14.
//  Copyright (c) 2014 Ivan Vasilevich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
