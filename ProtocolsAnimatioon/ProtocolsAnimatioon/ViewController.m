//
//  ViewController.m
//  ProtocolsAnimatioon
//
//  Created by Ivan Vasilevich on 11/23/14.
//  Copyright (c) 2014 Ivan Vasilevich. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()<UICollisionBehaviorDelegate>

@property (nonatomic, strong) IBOutlet UIView *gameView;
@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIGravityBehavior *gravity;
@property (nonatomic, strong) UICollisionBehavior *collider;
@property (nonatomic, strong) AVAudioPlayer *bgMusicPlayer;
@property (nonatomic, strong) AVAudioPlayer *seMusicPlayer;


@end

@implementation ViewController

#define MAX_SQUARE_SIDE_LENGHT 28

- (UICollisionBehavior *)collider{
    if (!_collider) {
        _collider = [[UICollisionBehavior alloc] init];
        [self.animator addBehavior:_collider];
        _collider.translatesReferenceBoundsIntoBoundary = YES;
        _collider.collisionDelegate = self;
    }
    return _collider;
}

- (UIDynamicAnimator *)animator{
    if (!_animator) {
        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.gameView];
    }
    return _animator;
}

- (UIGravityBehavior *)gravity{
    if (!_gravity) {
        _gravity = [[UIGravityBehavior alloc] init];
        [self.animator addBehavior:_gravity];
        if ([self.drugs  containsObject:@"MDMA"]) {
            _gravity.magnitude -= .9;
        }
    }
    return _gravity;
}

- (void)addRandomSquare{
    
    CGSize squareSize = CGSizeMake(arc4random()%MAX_SQUARE_SIDE_LENGHT+1,
                                   arc4random()%MAX_SQUARE_SIDE_LENGHT+1);
    CGFloat xPos = arc4random()%(int)self.view.frame.size.width;
    
    UIView *square = [[UIView alloc] initWithFrame:CGRectMake(xPos, 0, squareSize.width, squareSize.height)];
    square.backgroundColor = [self randomColor];
    
//    self.animato
    [self.gameView addSubview:square];
    [self.gravity addItem:square];
    [self.collider addItem:square];
    
    
    
    
}


- (UIColor *)randomColor{
    NSArray *colors = @[[UIColor redColor], [UIColor grayColor], [UIColor cyanColor],
                        [UIColor purpleColor], [UIColor clearColor], [UIColor orangeColor],
                        [UIColor greenColor]];
    return [colors objectAtIndex:arc4random()%[colors count]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    [self playMusic];
    
    
    UIView *redVeiw = [[UIView alloc] initWithFrame:CGRectMake(20, 10, 50, 36)];
    redVeiw.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:redVeiw];
    
    
    UIView *greenView = [[UIView alloc] initWithFrame:CGRectMake(200, 100, 120, 99)];
    greenView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:greenView];
    
    [UIView animateWithDuration:3 animations:^{
        greenView.frame = CGRectMake(30, 200, 200, 200);
        redVeiw.transform = CGAffineTransformMakeRotation(2);
    }];
    
    
    [self.view addSubview:self.gameView];
    
    NSTimeInterval interval = 1.1;
    
    
    if ([self.drugs containsObject:@"Mushrooms"]) {
        interval += 1;
    }
    
    if ([self.drugs containsObject:@"LSD"]) {
        interval -= 1;
    }
    
    [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(addRandomSquare) userInfo:nil repeats:YES];


}


- (void)playMusic{
    
    NSURL *bgMusicURL = [[NSBundle mainBundle] URLForResource:@"12 - Dare" withExtension:@"mp3"];
    self.bgMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:bgMusicURL error:nil];
    [self.bgMusicPlayer prepareToPlay];
    [self.bgMusicPlayer play];
    
}


- (void)playSE{
    
    if (![self.seMusicPlayer isPlaying]) {
        NSURL *bgMusicURL = [[NSBundle mainBundle] URLForResource:@"pew-pew-lei" withExtension:@"caf"];
        self.seMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:bgMusicURL error:nil];
        [self.seMusicPlayer prepareToPlay];
        [self.seMusicPlayer play];
    }
    
   
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.bgMusicPlayer = nil;
    self.seMusicPlayer = nil;
    [self.animator removeAllBehaviors];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
}


- (void)collisionBehavior:(UICollisionBehavior*)behavior beganContactForItem:(id <UIDynamicItem>)item1 withItem:(id <UIDynamicItem>)item2 atPoint:(CGPoint)p{
    [self playSE];
//    [self.collider removeItem:item1];
//    [self.gravity removeItem:item1];
//    UIView *it1 = (UIView *)item1;
//    [it1 removeFromSuperview];
    
}


@end
