//
//  DrugsTableViewController.m
//  ProtocolsAnimatioon
//
//  Created by Ivan Vasilevich on 11/30/14.
//  Copyright (c) 2014 Ivan Vasilevich. All rights reserved.
//

#import "DrugsTableViewController.h"
#import "ViewController.h"
@interface DrugsTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *drugsTable;
@property (nonatomic, strong) NSArray *drugs;
@property (nonatomic, strong) NSMutableArray *selectedDrugs;

@end

@implementation DrugsTableViewController


- (NSMutableArray *)selectedDrugs{
    if (!_selectedDrugs) {
        _selectedDrugs = [NSMutableArray new];
    } return _selectedDrugs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.drugs = @[@"LSD", @"Nikotin", @"Cofeein", @"Cocain", @"Mushrooms", @"Travmadol", @"MDMA", @"Kley", @"PurpleDrink", @"Piracetam", @"OXY", @"Spice"];
    // Do any additional setup after loading the view.
    self.drugsTable.dataSource = self;
    self.drugsTable.delegate = self;
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.drugs count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.drugsTable dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = [self.drugs objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = @"";
    
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%i", indexPath.row);
    if (![self.selectedDrugs containsObject:self.drugs[indexPath.row]]) {
        [self.selectedDrugs addObject:self.drugs[indexPath.row]];
    }
    
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[ViewController class]]) {
        ViewController *VC = segue.destinationViewController;
        VC.drugs = self.selectedDrugs;
    }
}


@end
