//
//  ViewController.swift
//  G64L15
//
//  Created by Ivan Vasilevich on 8/22/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}
	
	@IBAction func tapRecognized(_ sender: UITapGestureRecognizer) {
		print("recognizerView's tag =", sender.view!.tag)
	}
	
	@IBAction func panRecognized(_ sender: UILongPressGestureRecognizer) {
		print(sender.state.rawValue)
		let location = sender.location(in: view)
		sender.view?.frame.origin = location
	}
	
}

