//
//  MasterViewController.swift
//  G63L12
//
//  Created by Ivan Vasilevich on 7/8/18.
//  Copyright © 2018 RockSoft. All rights reserved.
// b4684820@nwytg.net

//http://docs.parseplatform.org/ios/guide/

import UIKit
import Parse
import ParseUI

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [PFObject]()


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
			
			
		}
		fetchData()
		
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if PFUser.current()?.username == nil {
			login()
		}
	}
	
	func login() {
		let loginVC = PFLogInViewController()
		loginVC.delegate = self
		present(loginVC, animated: true, completion: nil)
	}
	
	@objc
	func insertNewObject(_ sender: Any) {

		if UIDevice.current.userInterfaceIdiom == .phone {
			
		}
		
		let alert = UIAlertController(title: "New Post?", message: "DA", preferredStyle: .alert)
		let addNewPostAction = UIAlertAction(title: "Add", style: .default) { (action) in
			let newPost = PFObject(className: "Post")
			if let user = PFUser.current(), let userName = user.username {
				newPost["authorName"] = userName
				newPost["author"] = user
			}
			
			
			newPost["text"] = alert.textFields?.first?.text?.isEmpty ?? true ? "chuvak ne mapisal" : alert.textFields!.first!.text!
			newPost.saveEventually({ (success, error) in
				if success {
					self.fetchData()
				}
				else {
					print(error!)
				}
			})
			
		}
		alert.addAction(addNewPostAction)
		alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
		alert.addTextField { (textField) in
			textField.placeholder = "krokodil 4000"
		}
		present(alert, animated: true, completion: nil)
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row]
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row]
		cell.textLabel!.text = object["text"] as? String
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}
	
	func fetchData() {
		let query = PFQuery.init(className: "Post")
		//		query.whe
		query.findObjectsInBackground { (objects, error) in
			if let objects = objects {
				self.objects = objects
				self.tableView.reloadData()
			}
		}
	}

}

extension MasterViewController: PFLogInViewControllerDelegate {
	func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
		logInController.dismiss(animated: true, completion: nil)
	}
}

