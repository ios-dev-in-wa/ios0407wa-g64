//
//  PhoneInputVC.swift
//  G63L12
//
//  Created by Ivan Vasilevich on 8/17/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import AccountKit


class PhoneInputVC: UIViewController{
	
	var _accountKit: AKFAccountKit!
	var _pendingLoginViewController: AKFViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

	@IBAction func showSMSVerification(_ sender: UIButton) {
		// Do any additional setup after loading the view.
				_accountKit = AKFAccountKit.init(responseType: .accessToken)
				let number = AKFPhoneNumber.init(countryCode: "+380", phoneNumber: "")
		//		let number = AKFPhoneNumber.init(countryCode: "+380", phoneNumber: "930736656")
				_pendingLoginViewController = _accountKit.viewControllerForPhoneLogin(with: number, state: "Ukraine")
//		        _pendingLoginViewController.uiManager = self
		        _pendingLoginViewController.delegate = self
		//		_pendingLoginViewController = _accountKit.viewControllerForPhoneLogin()
				//_accountKit.viewControllerForEmailLogin(withEmail: "ua2345@gmail.com", state: "Ukraine")
		
		
//		        spinner.startAnimating()
		        sender.isEnabled = false
//				present(_pendingLoginViewController as! UIViewController, animated: true, completion:
		present(_pendingLoginViewController as! UIViewController, animated: true) {
			sender.isEnabled = true
		}
	
	}
}

extension PhoneInputVC: AKFViewControllerDelegate {
	func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
		viewController.dismiss(animated: true, completion: nil)
	}
	
	func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
		print(code)
		viewController.dismiss(animated: true, completion: nil)
	}
}
