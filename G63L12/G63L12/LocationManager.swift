//
//  LocationManager.swift
//  G63L12
//
//  Created by Ivan Vasilevich on 8/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import CoreLocation

let newLocationAvailableNotification = "newLocationAvailableNotification"
let kLocation = "kLocation"

class LocationManager: NSObject {
	
	static let shared = LocationManager()
	
	var manager = CLLocationManager()
	
	var lastLocation: CLLocation?
	
	override init() {
		super.init()
		if CLLocationManager.authorizationStatus() == .notDetermined {
			manager.requestWhenInUseAuthorization()
		}
		manager.delegate = self
	}
	
	func startUpdateLocation() {
		manager.startUpdatingLocation()
	}
	
	func stopUpdateLocation() {
		manager.stopUpdatingLocation()
	}

}

extension LocationManager: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		print(locations.last!)
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: newLocationAvailableNotification), object: self, userInfo: [kLocation: locations.last!])
	}
}
