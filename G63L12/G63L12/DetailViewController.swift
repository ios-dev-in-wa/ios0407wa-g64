//
//  DetailViewController.swift
//  G63L12
//
//  Created by Ivan Vasilevich on 7/8/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Parse

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!

	var detailItem: PFObject? {
		didSet {
			// Update the view.
			configureView()
		}
	}
	

	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail["text"] as? String
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		configureView()
	LocationManager.shared.startUpdateLocation()
		
		NotificationCenter.default.addObserver(self, selector: #selector(saveLocationToServer(notification:)), name: NSNotification.Name(rawValue: newLocationAvailableNotification), object: nil)
		
	}
	
	@objc func saveLocationToServer(notification: Notification) {
		if let location = notification.userInfo?[kLocation] as? CLLocation {
			let pfobject = PFObject(className: "Location")
			let parseLocation = PFGeoPoint(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
			pfobject["coordinate"] = parseLocation
			pfobject.saveEventually()
			
			
		}
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}

	deinit {
		print("deinit")
	}

}

