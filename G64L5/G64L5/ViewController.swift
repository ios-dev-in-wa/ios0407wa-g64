//
//  ViewController.swift
//  G64L5
//
//  Created by Ivan Vasilevich on 7/18/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	var callCount = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		arrays()
		dictionarys()
		drawBox()
		let a = "q54"
		let b = Int(a)
		print("number form b = \(b)")
//		let c = b! + 5
		let d = b ?? 0 + 5
		if let realB = b { //b?.op1?.op2 ?? newOnOffButton
//			if 3== 9 {
//				if false == false {
//					print("number form b = \(realB)")
//				}
//			}
		}
		else {
			print("enter correct number")
		}
		
		foo()
		
		addTwoNumbers(number: nil)
		addTwoNumbers(number: 3)
		addTwoNumbers(number: 5.25)
		
		for i in 0..<34 {
			addTwoNumbers(number: Double(i))
		}
		

		
	}
	
	func addTwoNumbers(number: Double?) {
		callCount += 1
		print("func addTwoNumbers called #\(callCount)")
		guard let realNumber = number else {
			print("enter correct number")
			return
		}
		let realNumberString = String(format: "%.0f", realNumber)
		guard let intFromStringWithrRealNumber = Int(realNumberString) else {
			print("intFromStringWithrRealNumber = nil")
			return
		}
		print("intFromStringWithrRealNumber = ", intFromStringWithrRealNumber)
		print("number form number = \(realNumber)")
		
	}
	
	func foo() {
		callCount -= 100500
	}

	func arrays() {
		var numbers = [1, 2, 3, 4, 31]
		print("numbers = ", numbers)
		print("count of elements in numbes =", numbers.count)
		print("last member of numbers =", numbers[numbers.count - 1])
		if !numbers.isEmpty {
			print("last member of numbers =", numbers.first!)
			print("last member of numbers =", numbers.last!)
		}
		numbers.append(368)
		print("numbers = ", numbers)
		print(numbers.popLast())
		print(numbers.popLast())
		print(numbers.remove(at: 1))
		numbers.insert(256, at: 1)
		numbers[3] = 1234
		let result = numbers[0] + numbers[1]
		print("numbers = ", numbers)
		let name = "Ivan"
		let longString = """
Created by Ivan Vasilevich on 7/18/18.
//  Copyright © 2018 RockSoft. All rights reserved.
"""
	let words = longString.components(separatedBy: " ")
		if words.contains(name) {
			print("\(name.uppercased()) В ЗДАНИИ")
		}
		else {
			print(name + "  !В ЗДАНИИ".lowercased())
		}
		
		for _ in 0..<15 {
			numbers.append(Int(arc4random()%10))
		}
		print(numbers)
		
		let arrayOfSomething = [1, "1"] as [Any]
		let element = arrayOfSomething[0]
		
		if let intElelment = element as? Int {
			intElelment
		}
	}
	
	func dictionarys() {
		var phoneBook = [String : Int].init()
		phoneBook = ["ambulance": 	103,
					 "gas"		:	104,
					 "police" 	:	101]
		print(phoneBook["1gas"] ?? "no number found")
		phoneBook["12345"] = 12345
		phoneBook["ambulance"] = 1033
		
		var str = ""//String.init()
		str = "abcd\(Int.init())"
		print(str)
	}
	
	func drawBox() {
		let rect = CGRect.init(x: 10, y: 100, width: 32, height: 32)
		let box = UIView(frame: rect)
//		rgb(232, 104, 58)
		let color = UIColor.init(red: 232/255.0, green: 104/255.0, blue: 58/255.0, alpha: 1)
		box.backgroundColor = color
		box.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									  green: CGFloat(arc4random_uniform(256))/255,
									  blue: CGFloat(arc4random_uniform(256))/255,
									  alpha: 1)
		view.addSubview(box)
		
	}
}

