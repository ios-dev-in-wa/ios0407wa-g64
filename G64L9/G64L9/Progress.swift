//
//  Progress.swift
//  G64L9
//
//  Created by Ivan Vasilevich on 8/1/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Progress: NSObject {
	var participantName: String
	var stages: [Stage]
	var totalProgressPercent: Double {
//		stages.count
		let completeStages = stages.filter { (st) -> Bool in
			return st.isComplete
		}
		return (completeStages.count.double / stages.count.double) * 100
	}
	
	init(participantName: String, stages: [Stage]) {
		self.participantName = participantName
		self.stages = stages
	}
	
}

struct Stage {
	var name: String
	var isComplete: Bool = false
	
	init(_ name: String) {
		self.name = name
	}
}

extension Int {
	var double: Double {
		return Double(self)
	}
}
