//
//  ProgressDetailVC.swift
//  G64L9
//
//  Created by Ivan Vasilevich on 8/1/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ProgressDetailVC: UIViewController {
	
	var elementToDisplay: Progress!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		navigationItem.title = elementToDisplay.participantName
		
		for i in 0..<elementToDisplay.stages.count {
			let tag = (i + 1) * -1
			if let label = view.viewWithTag(tag) as? UILabel {
				label.text = elementToDisplay.stages[i].name + ": " + (elementToDisplay.stages[i].isComplete ? "+" : "-")
			}
		}
    }

	@IBAction func deleteRecord(_ sender: UIBarButtonItem) {
		navigationController?.popViewController(animated: true)
	}
}
