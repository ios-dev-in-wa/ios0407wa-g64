//
//  ViewController.swift
//  G64L9
//
//  Created by Ivan Vasilevich on 8/1/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	var studentsProgress = [Progress]()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		fetchData()
	}
	
	func fetchData() {
		let stage1 = Stage("Robot")
		let stage2 = Stage("Arithmetic")
		let stage3 = Stage("Strings")
		
		let stages = [stage1, stage2, stage3]
		
		let vaninProgress = Progress(participantName: "Vania", stages: stages)
		vaninProgress.stages[0].isComplete = true
		let vikinProgress = Progress(participantName: "Vika", stages: stages)
		print(vikinProgress.stages[0].isComplete)
		let verinProgress = Progress(participantName: "Vera", stages: stages)
		studentsProgress = [verinProgress, vaninProgress, vikinProgress]
		
		for i in 0..<studentsProgress.count {
			let tag = (i + 1) * -1
			if let label = view.viewWithTag(tag) as? UILabel {
				label.text = studentsProgress[i].totalProgressPercent.description
			}
		}
		
	}

	@IBAction func studentSelected(_ sender: UIButton) {
		let elementToDisplay = studentsProgress[sender.tag]
		performSegue(withIdentifier: "showLooser", sender: elementToDisplay)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let progDetailVC = segue.destination as? ProgressDetailVC,
			let progressToDisplay = sender as? Progress {
			progDetailVC.elementToDisplay = progressToDisplay
		}
	}
	
	
}

