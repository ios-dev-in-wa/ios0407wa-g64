//
//  ViewController.swift
//  G64L7
//
//  Created by Ivan Vasilevich on 7/26/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var startTimerButton: UIButton!
	@IBOutlet weak var timerLabel: UILabel!
	
	var raceTimer: Timer?
	var startDate: Date?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let kRunCount = "kRunCount"
		var runCount = UserDefaults.standard.integer(forKey: kRunCount)
		runCount += 1
		timerLabel.text = "run#" + runCount.description
		UserDefaults.standard.set(runCount, forKey: kRunCount)
		
	}
	
	@IBAction func startButtonPressed(_ sender: UIButton) {

		print(sender.currentTitle ?? "no title")

		if startTimerButton.tag == 0 {
			raceTimer?.invalidate()
			raceTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
			startDate = Date()
			startTimerButton.setTitle("Stop", for: .normal)
			startTimerButton.tag = 1
		}
		else {
			raceTimer?.invalidate()
			startTimerButton.setTitle("Start", for: .normal)
			startTimerButton.tag = 0
		}
		

	}
	
	@objc func tick() {
		guard let startDate = self.startDate else {
			print("no start date")
			return
		}
		timerLabel.text = (-startDate.timeIntervalSinceNow).description
	}
	

}

