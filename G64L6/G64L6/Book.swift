//
//  Book.swift
//  G64L6
//
//  Created by Ivan Vasilevich on 7/23/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Book: NSObject {
	
	enum PageTurnDirrection: Int {
		case forward = 1
		case backward = -1
	}
	
	
	let authorName: String
	let price: Double
	private var currentPage = 0
	var isPurchased = false
	
	override var description: String {
		return """
\(super.description)
		authorName: \(authorName)
		currentPage: \(currentPage)
		purchased: \(isPurchased)
"""
	}
	
	init(authorName: String, price: Double) {
		self.authorName = authorName
		self.price = price
	}
	
	func turnPage(turnDirrection: PageTurnDirrection = .forward) {
		var addCount = 0
		if turnDirrection == .forward {
			addCount = +1
		}
		else {
			addCount = -1
		}
		addCount = turnDirrection == .forward ? +1 : -1
		currentPage += addCount
	}
	
	func foo() -> String  {
		guard let a = Int("aaaa5") else {
			return "error"
		}
		return "result"
		
		
	}
	

}
