//
//  ViewController.swift
//  G64L6
//
//  Created by Ivan Vasilevich on 7/23/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var centerLabel: UILabel!
	var myBook = Book.init(authorName: "AZAZAS", price: 200)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
//		if let myBook = myBook {
			myBook.turnPage()
			//		myBook.description = "fddsfdsfdsf"
			//		myBook.turnPage(isForward: false)
			myBook.turnPage(turnDirrection: .forward)
			myBook.turnPage(turnDirrection: .backward)
			//		myBook.currentPage
			print("my book \"\(myBook.description)\"")
//		}
//		myBook = nil
		print([1,4,8].description)
		
		let fooObject = Foo.init()
		print(fooObject)
		
		let my2book = Book(authorName: "qwertyuiop", price: 99)
		let my3book = Book(authorName: "qwertyuiop1234", price: 999)
		let allBooks = [myBook, my2book, my3book]
		let allBooksAuthors = allBooks.map { (singleBook) -> String in
			return singleBook.authorName
		}
		let allBooksAuthors2 = allBooks.map { $0.authorName } as NSArray
		allBooksAuthors2.write(toFile: "/Users/ivanvasilevich/Desktop/authorsNames.plist", atomically: false)
	}
	

	@IBAction func displayGreetings() {
		let textToDisplay = "MAMA YA ZDELAL"
		print(textToDisplay)
		centerLabel.text = textToDisplay
		//		view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
		//									   green: CGFloat(arc4random_uniform(256))/255,
		//									   blue: CGFloat(arc4random_uniform(256))/255,
		//									   alpha: 1)
		
	}
}

class Foo {
	var bar = 0
}

